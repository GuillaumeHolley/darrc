#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int main_kmc(int argc, char* argv[]);
int main_kmc_dump(int argc, char* argv[]);

#ifdef __cplusplus
}
#endif
