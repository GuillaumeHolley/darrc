#pragma once

/* ===================================================================================================================================
*  INCLUDES AND DEFINES
*  ===================================================================================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "CC.h"
#include "UC_annotation.h"
#include "bft.h"

#define CC_VERTEX 1
#define UC_VERTEX 2
#define UC_PRESENT 4

void write_kmers_2disk(BFT_Root* root, char* filename, bool compressed_output);

void write_BFT_Root_sparse(BFT_Root*  root, char* filename, bool write_root_only);
void write_Node_sparse(Node*  node, BFT_Root* root, int lvl_node, FILE* file, int size_kmer);
void write_UC_sparse(UC* uc, BFT_Root* root, FILE* file, int size_substring, uint16_t nb_children, bool compressed_header);
void write_CC_sparse(CC*  cc, BFT_Root* root, int lvl_cc, FILE* file, int size_kmer);

BFT_Root* read_BFT_Root_sparse(char* filename);
BFT_Root* read_BFT_Root_sparse_offset(char* filename, long int offset_read);
void read_Node_sparse(Node*  node, BFT_Root* root, int lvl_node, FILE* file, int size_kmer);
void read_UC_sparse(UC* uc, BFT_Root* root, FILE* file, int size_substring, uint16_t nb_children);
void read_CC_sparse(CC*  cc, BFT_Root* root, int lvl_cc, FILE* file, int size_kmer);


void l_write_BFT_Root(BFT_Root*  root, char* filename_prefix, int cut_lvl, bool write_below_cut_lvl);
void l_write_Node(Node*  node, BFT_Root* root, int lvl_node, int size_kmer, int cut_lvl,
                  bool write_below_cut_lvl, FILE* file, char* filename_prefix);
void l_write_CC(CC*  cc, BFT_Root* root, int lvl_cc, int size_kmer, int cut_lvl, bool write_below_cut_lvl,
                FILE* file, char* filename_prefix);

void write_annotation_array_elem(char* filename, annotation_array_elem* annot_sorted, int size_array);
void read_annotation_array_elem(char* filename, annotation_array_elem** annot_sorted, int* size_array);

void write_BFT_Root_sparse2(BFT_Root* root, char* filename, bool write_root_only);
BFT_Root* read_BFT_Root_sparse2(char* filename_bft_old, char* filename_new_comp_colors, Pvoid_t* PJArray, int len_longest_annot);
void read_Node_sparse2(Node* node, BFT_Root* root, int lvl_node, FILE* file, int size_kmer, Pvoid_t* PJArray);
void read_CC_sparse2(CC* cc, BFT_Root* root, int lvl_cc, FILE* file, int size_kmer, Pvoid_t* PJArray);

inline uint16_t get_header_node_file(Node* node_array, int position, int nb_skp, char* filename_prefix, bool cut_lvl){

    if (cut_lvl){

        FILE* file;

        uint16_t header;

        int len_filename = strlen(filename_prefix);

        char* new_filename = malloc((len_filename + 30) * sizeof(char));
        ASSERT_NULL_PTR(new_filename, "get_header_node_file()\n");

        strcpy(new_filename, filename_prefix);
        new_filename[len_filename] = '_';

        sprintf(&(new_filename[len_filename+1]), "%d", nb_skp+position);

        file = fopen(new_filename, "r");
        ASSERT_NULL_PTR(file, "get_header_node_file()\n");

        free(new_filename);

        if (fread(&header, sizeof(uint16_t), 1, file) != 1) ERROR("get_header_node_file()\n");

        fclose(file);

        return header;
    }
    else return node_array[position].UC_array.nb_children;
}

BFT_Root* l_read_BFT_Root(char* filename_prefix, int cut_lvl);
void l_read_Node(Node*  node, BFT_Root* root, int lvl_node, int cut_lvl, FILE* file, char* filename_prefix,
                 int size_kmer);
void l_read_CC(CC*  cc, BFT_Root* root, int lvl_cc, int lvl_cut, FILE* file, char* filename_prefix,
               int size_kmer);

void read_cut_BFT_Root(char* input_filename, char* output_filename, int cut_lvl, bool pack_in_subtries);
void read_cut_Node(FILE* file_input, FILE* file_output, char* filename_output, int size_kmer, int lvl_node,
                   int cut_lvl, bool pack_in_subtries, info_per_level* info_per_lvl);
void read_cut_UC(FILE* file_in, FILE* file_out, int size_substring, uint16_t nb_children);
void read_cut_CC(FILE* file_input, FILE* file_output, char* filename_output, int size_kmer, int lvl_cc,
                 int cut_lvl, bool pack_in_subtries, info_per_level* info_per_lvl);

