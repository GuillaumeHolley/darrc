#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <math.h>

#include "useful_macros.h"
#include "UC_annotation.h"
#include "marking.h"
